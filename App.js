import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from './screens/HomeScreen';
import CameraScreen from './screens/CameraScreen';

const Stack = createStackNavigator();

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>


        {/*<Stack.Screen name="CamApp"  component={CamApp}  options={{ title: 'CamApp' }}   /> */}
         <Stack.Screen name="HomeScreen"  component={HomeScreen}  options={{ title: 'Home' }} />
         <Stack.Screen name="CameraScreen"  component={CameraScreen}  options={{ title: 'Camera' }}   />
                  
      </Stack.Navigator>
     </NavigationContainer> 

  );
};

export default App;
