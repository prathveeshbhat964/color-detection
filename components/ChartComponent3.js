import React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import { LineChart } from 'react-native-charts-wrapper';
import Utilities from './Utilities';

const ChartComponent3 = ({ data }) => {
  
  const dataPoints = data.map(item => ({
    x: item.CircleNo,
    y: parseFloat(item.E),
  }));
  
  const { m, c } = Utilities.linearRegression(
    dataPoints.map(item => item.x),
    dataPoints.map(item => item.y)
  );

  const dataSets = [{
    label: 'Conc. Vs △E',
    values: dataPoints.map(point => ({
      x: point.x,
      y: point.y
    }))
  }];

  return (
    <View style={styles.container}>
      <Text style={styles.heading }>Line Chart</Text>
      <LineChart
        style={styles.chart}
        data={{ dataSets }}
        chartDescription={{ text: 'Known Values' }} // to remove chart description
        drawMarkers={true} // to remove markers
        xAxis={{
            
          drawAxisLine: true, 
          drawGridLines: true 
        }}
        yAxis={{
          drawAxisLine: true, 
          drawGridLines: false 
        }}
        config={{
            line: {
            colors: ['black', 'blue'] // Change to desired color, e.g., black
            }
        }}
      />
      <Text style={styles.equation}>f(x)={m}x + {c} </Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    marginTop:20,
    marginBottom: 50,
    backgroundColor: '#FFFFFF',
    width: 400,
    height: 300,
  },
  chart: {
    flex: 1,
    width: 380,
    backgroundColor:'#FFFFFF',
    height: 220,
    alignSelf:'center',
  },
   heading: {
    alignSelf:'flex-start',
    fontSize: 24,
    fontWeight: 'bold',
    color:'black',
    marginBottom: 10,
    paddingLeft:5,
  },
  equation:{
    fontSize:15,
    textAlign:'center',
    fontWeight:"800",
    padding:5,
  
  }
});

export default ChartComponent3;
