import React from 'react';
import { View,StyleSheet, Text} from 'react-native';
import { LineChart } from 'react-native-chart-kit';
import Utilities from '../components/Utilities';

const ChartComponent = ({ data }) => {

  // Extract CircleNo and DeltaE values from data
   const circleNum = data.map(item => item.CircleNo);
   const circleNumbers = [2,4,6,8]
   const deltaEValues = data.map(item => parseFloat(item.E));
   const {m,c}=Utilities.linearRegression(circleNum,deltaEValues);
  
  // Chart data
  const chartData = {
    labels: circleNumbers,
    datasets: [
      {
        data: deltaEValues,
        //  color: (opacity = 1) => `rgba(134, 65, 244, ${opacity})`, // optional

      },
    
    ],
     legend: ["Conc. Vs △E"]
  };

  return (
    <View  style={styles.container}>
      <Text style={styles.heading }>Line Chart</Text>
       <View style={styles.bottom}></View>
      <LineChart
        data={chartData}
        width={400} // Adjust width as needed
        height={220} // Adjust height as needed
        yAxisSuffix="" // Suffix for yAxis values
        yAxisInterval={2}
        chartConfig={{
          backgroundGradientFrom: '#fff',
          backgroundGradientTo: '#fff',
          color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`, // Line color
          labelColor: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`, // Label color
          strokeWidth: 2, // Line width
        }}
       // bezier // Smooth line
      />
     <View style={styles.bottom}>
     <Text style={styles.bottomText}>Conc. Vs △E</Text>
     <Text style={styles.equation}>f(x)={m}x + {c} </Text>
     </View>
    </View>
  );
};

const styles = StyleSheet.create({
    container:{
    //marginTop:50,
   marginBottom:10,
   },
    heading: {
    alignSelf:'flex-start',
    fontSize: 24,
    fontWeight: 'bold',
    marginTop: 20,
    color:'black',
    marginBottom: 10,
  },
  bottomText:{
     alignSelf:'center',
    fontSize: 18,
    fontWeight: 'bold',
    color:'black',
    padding:10,
    backgroundColor:'white'
  },
  bottom:{
    backgroundColor:'white',
    height:80,
  },
  equation:{
    fontSize:15,
    textAlign:'center',
    fontWeight:"800",
  
  }
 
})
export default ChartComponent;
