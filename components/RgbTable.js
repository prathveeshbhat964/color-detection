import React, { useState } from 'react';
import { View, Text, StyleSheet, TextInput, ScrollView } from 'react-native';

const RgbTable = ({ rgbtableData ,known}) => {
     const generateHardcodedData = () => {
    // Generate hardcoded data for the given number of rows
    const length = known ? 5 : 2; 
    return Array.from({length}, (_, index) => ({
      groupNumber: index + 1,
      r: '0',
      g: '0',
      b: '0',
    }));
  };

  const displayData = rgbtableData.length > 0 ? rgbtableData : generateHardcodedData(); // Use tableData if provided, otherwise generate hardcoded data

  return (
    <View style={styles.container}>
      {/* Table Header */}
      <View style={styles.header}>
        <Text style={styles.columnHeader}>Group</Text>
        <Text style={styles.columnHeader}>R</Text>
        <Text style={styles.columnHeader}>G</Text>
        <Text style={styles.columnHeader}>B</Text>
      </View>

      {/* Table Body */}
      <ScrollView >
        {displayData.map((rowData, index) => (
          <View key={index} style={styles.row}>
            <Text style={styles.cell}>{rowData.groupNumber}</Text>
            <Text style={styles.cell}>{rowData.r}</Text>
            <Text style={styles.cell}>{rowData.g}</Text>
            <Text style={styles.cell}>{rowData.b}</Text>
          </View>
        ))}
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
   // flex: 1,
    backgroundColor: '#000',
  },
  header: {
    flexDirection: 'row',
    backgroundColor: '#f2f2f2',
    borderBottomWidth: 1,
    borderBottomColor: '#fff',
    paddingVertical: 10,
    color:'white',
    alignItems: 'center',
    justifyContent: 'center',
  },
  columnHeader: {
    flex: 1,
    textAlign: 'center',
    fontWeight: 'bold',
  },
  row: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
    paddingVertical: 5,
    alignItems: 'center',
    height: 49,
  },
  cell: {
    color:'white',
    flex: 1,
    textAlign: 'center',
  },
  input: {
    flex: 1,
    textAlign: 'center',
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 5,
    color:'white', 
    marginHorizontal: 5,
    paddingVertical: 5,
  },
});

export default RgbTable;
