
 function hexToHSL(hex) {
  // Remove '#' if it exists
  hex = hex.replace('#', '');

  // Parse hex to RGB
  const r = parseInt(hex.substring(0, 2), 16) / 255;
  const g = parseInt(hex.substring(2, 4), 16) / 255;
  const b = parseInt(hex.substring(4, 6), 16) / 255;

  // Find greatest and smallest channel values
  const max = Math.max(r, g, b);
  const min = Math.min(r, g, b);

  // Calculate lightness
  let l = (max + min) / 2;

  // Calculate saturation
  let s = 0;
  if (max !== min) {
      s = l > 0.5 ? (max - min) / (2 - max - min) : (max - min) / (max + min);
  }

  // Calculate hue
  let h = 0;
  if (max === r) {
      h = (g - b) / (max - min) + (g < b ? 6 : 0);
  } else if (max === g) {
      h = (b - r) / (max - min) + 2;
  } else {
      h = (r - g) / (max - min) + 4;
  }
  h /= 6;

  // Convert hue to degrees
  h *= 360;

  // Round values and return as object
  return { h: Math.round(h), s: Math.round(s * 100), l: Math.round(l * 100) };
}


 function calculateAverageColor(colors)  {
  // Separate the red, green, and blue components
  const reds = colors.map(color => parseInt(color.substring(1, 3), 16));
  const greens = colors.map(color => parseInt(color.substring(3, 5), 16));
  const blues = colors.map(color => parseInt(color.substring(5, 7), 16));

  // Calculate the average of each component
  const avgRed = Math.round(reds.reduce((a, b) => a + b, 0) / colors.length);
  const avgGreen = Math.round(greens.reduce((a, b) => a + b, 0) / colors.length);
  const avgBlue = Math.round(blues.reduce((a, b) => a + b, 0) / colors.length);

  // Convert the average components back to hexadecimal
  const avgHex = `#${avgRed.toString(16).padStart(2, '0')}${avgGreen.toString(16).padStart(2, '0')}${avgBlue.toString(16).padStart(2, '0')}`;

  return avgHex;
};


function hexToRgb(hex) {
  // Remove '#' if it exists
  hex = hex.replace('#', '');
//console.log('******hex.replace called**** ')
  // Parse hex to RGB
  const r = parseInt(hex.substring(0, 2), 16);
  const g = parseInt(hex.substring(2, 4), 16);
  const b = parseInt(hex.substring(4, 6), 16);

  // Return RGB values as an array
  return [r, g, b];
}



function rgb2lab(rgb){
  var r = rgb[0] / 255,
      g = rgb[1] / 255,
      b = rgb[2] / 255,
      x, y, z;

  r = (r > 0.04045) ? Math.pow((r + 0.055) / 1.055, 2.4) : r / 12.92;
  g = (g > 0.04045) ? Math.pow((g + 0.055) / 1.055, 2.4) : g / 12.92;
  b = (b > 0.04045) ? Math.pow((b + 0.055) / 1.055, 2.4) : b / 12.92;

  x = (r * 0.4124 + g * 0.3576 + b * 0.1805) / 0.95047;
  y = (r * 0.2126 + g * 0.7152 + b * 0.0722) / 1.00000;
  z = (r * 0.0193 + g * 0.1192 + b * 0.9505) / 1.08883;

  x = (x > 0.008856) ? Math.pow(x, 1/3) : (7.787 * x) + 16/116;
  y = (y > 0.008856) ? Math.pow(y, 1/3) : (7.787 * y) + 16/116;
  z = (z > 0.008856) ? Math.pow(z, 1/3) : (7.787 * z) + 16/116;

  return [(116 * y) - 16, 500 * (x - y), 200 * (y - z)].map(value => parseFloat(value.toFixed(2)));
}


function deltaE(color1, color2) {
  const deltaL = color2.l - color1.l;
  const deltaA = color2.a - color1.a;
  const deltaB = color2.b - color1.b;
  const deltaE = Math.sqrt(deltaL * deltaL + deltaA * deltaA + deltaB * deltaB);
  return deltaE.toFixed(2);
}

// Perform linear regression to calculate slope (m) and y-intercept (c)
const linearRegression = (xValues, yValues) => {
  const n = xValues.length;
  let sumX = 0;
  let sumY = 0;
  let sumXY = 0;
  let sumXSquare = 0;

  for (let i = 0; i < n; i++) {
    sumX += xValues[i];
    sumY += yValues[i];
    sumXY += xValues[i] * yValues[i];
    sumXSquare += xValues[i] * xValues[i];
  }

  const meanX = sumX / n;
  const meanY = sumY / n;

  const m = (n * sumXY - sumX * sumY) / (n * sumXSquare - sumX * sumX);
  const c = meanY - m * meanX;

  return { m: parseFloat(m.toFixed(2)), c: parseFloat(c.toFixed(2)) };
};



export default {
  hexToHSL,
  calculateAverageColor,
  hexToRgb,
  rgb2lab,
  deltaE,
  linearRegression
};