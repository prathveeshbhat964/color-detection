import React, { useState } from 'react';
import { View, Text, StyleSheet,  ScrollView } from 'react-native';

const Details = ({ tableData, known }) => {


  const generateHardcodedData = () => {
    // Generate hardcoded data for the given number of rows
    const length = known ? 15 : 9; 
    return Array.from({length}, (_, index) => ({
      No:index+1,
      CircleNo: index % 5 === 0 ? '0' : index % 5 === 1 ? '2' : index % 5 === 2 ? '4' : index % 5 === 3 ? 6 : '8', // Assign CircleNo based on index
      l: '0',
      a: '0',
      b: '0',
      b: '0',
      E: '0',
    }));
  };

  const displayData = tableData.length > 0 ? tableData.sort((a, b) => a.CircleNo - b.CircleNo) : generateHardcodedData(); // Use tableData if provided, otherwise generate hardcoded data

 

  return (
    <View style={styles.container}>
      {/* Table Header */}
      <View style={styles.header}>
        <Text style={styles.columnHeader}>No.</Text>
        {known&& <Text style={styles.columnHeader}>Conc.</Text> }
        <Text style={styles.columnHeader}>L*</Text>
        <Text style={styles.columnHeader}>A*</Text>
        <Text style={styles.columnHeader}>B*</Text>
        <Text style={styles.columnHeader}>△E</Text>
      </View>

      {/* Table Body */}
        {displayData.map((rowData, index) => (
          <View key={index} style={styles.row}>
            <Text style={styles.cell}>{index+1}</Text>
            {known&& <Text style={styles.cell}>{rowData.CircleNo}</Text>}
            <Text style={styles.cell}>{rowData.l}</Text>
            <Text style={styles.cell}>{rowData.a}</Text>
            <Text style={styles.cell}>{rowData.b}</Text>
            <Text style={styles.cell}>{rowData.E}</Text>
          </View>
        ))}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    //flex: 1,
    backgroundColor: '#000',
  },
  header: {
    flexDirection: 'row',
    backgroundColor: '#f2f2f2',
    borderBottomWidth: 1,
    borderBottomColor: '#fff',
    paddingVertical: 10,
    color:'white',
    alignItems: 'center',
    justifyContent: 'center',
  },
  columnHeader: {
    flex: 1,
    textAlign: 'center',
    fontWeight: 'bold',
  },
  row: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
    paddingVertical: 5,
    alignItems: 'center',
    height: 49,
  },
  cell: {
    color:'white',
    flex: 1,
    textAlign: 'center',
  },
  input: {
    flex: 1,
    textAlign: 'center',
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 5,
    color:'white', 
    marginHorizontal: 5,
    paddingVertical: 5,
  },
});

export default Details;
