import React,{useState, useEffect } from 'react';
import { View, Text, TouchableOpacity, StyleSheet,StatusBar } from 'react-native';
import { useRoute } from '@react-navigation/native';
import Details from '../components/Details';
import { ScrollView } from 'react-native-gesture-handler';
import RgbTable from '../components/RgbTable';
import ChartComponent from '../components/ChartComponent ';
import ChartComponent3 from '../components/ChartComponent3';

const HomeScreen = ({ navigation }) => {
  const route = useRoute();
  //const { knownTableData, unknownColorsData } = route.params;
  const [ knownTableData, setKnownTable]=useState([]);
  const [unknownColorsData, setUnknownColorsData]=useState([]);
 const [knownrgbtableData, setknownrgbtableData] = useState([]);
const [unknownrgbtableData, setunknownrgbtableData] = useState([]);

  const [tableData, setTableData]=useState([]);

  // useEffect(() => {
  //   if (route.params && route.params.knownTableData && route.params.unknownColorsData) {
  //     setKnownTable(route.params.knownTableData);
  //     setUnknownColorsData(route.params.unknownColorsData);
  //   }
  // }, [route.params]);

  console.log('Known Table Data:  in Home', knownTableData);
  console.log('Unknown Colors Data: in Home', unknownColorsData);

 
  const handleCameraPress = () => {
    navigation.navigate('CameraScreen', {
      knownTableData: knownTableData,
      unknownColorsData: unknownColorsData,
      knownrgbtableData:knownrgbtableData,
      unknownrgbtableData:unknownrgbtableData,
      setknownrgbtableData:setknownrgbtableData,
      setunknownrgbtableData:setunknownrgbtableData,
      setKnownTable: setKnownTable,
      setUnknownColorsData: setUnknownColorsData
    });
    
  };
  
// const openCam=()=>{
// navigation.navigate(CameraCapture)
//}
  

  return (
    <ScrollView style={styles.container}>
           <StatusBar barStyle="dark-content" />
     
      {/*  <TouchableOpacity style={styles.button} onPress={openCam}>
          <Text style={styles.buttonText}>Open Camera2 → </Text>
      </TouchableOpacity> */}
       <TouchableOpacity style={styles.button} onPress={handleCameraPress}>
              <Text style={styles.buttonText}>Open Camera → </Text>
      </TouchableOpacity>
     
      <Text style={styles.heading}>Known Values</Text>
      <Details tableData={knownTableData} known={true} />


      <Text style={styles.heading }>Unknown Values</Text>
      <Details tableData={unknownColorsData} known={false} />
      
      { knownTableData.length>0?  <ChartComponent data={knownTableData.slice(3)}  /> :null}
      { knownTableData.length>0? <ChartComponent3 data={knownTableData.slice(3)}/> :null}


      {/* <Text style={styles.heading }>Knwon RGB Table</Text>
      <RgbTable rgbtableData={knownrgbtableData}  known={true}/>

      <Text style={styles.heading }>Unknwon RGB Table</Text>
      <RgbTable rgbtableData={unknownrgbtableData} known={false} /> */}


    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 20,
    backgroundColor: '#FAD9BF',
  },
  button: {
    backgroundColor: 'black',
    padding: 10,
    width:'98%',
    borderRadius: 5,
    marginBottom: 5,
    marginTop:10
  },
  buttonText: {
    color: 'white',
    fontSize: 18,
    alignSelf:'center'
  },
  heading: {
    alignSelf:'flex-start',
    fontSize: 24,
    fontWeight: 'bold',
    marginTop: 20,
    color:'black',
    marginBottom: 10,
  },
});

export default HomeScreen;
