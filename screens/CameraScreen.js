import React, { useState, useEffect, useRef } from 'react';
import { View, Alert, Text,Button, ActivityIndicator , Image, TouchableOpacity, StyleSheet, Dimensions, SafeAreaView, StatusBar,Modal,ScrollView } from 'react-native';
import { Camera } from 'expo-camera';
import { MaterialIcons } from '@expo/vector-icons';
import { useNavigation ,useRoute } from '@react-navigation/native'; 
import { manipulateAsync, ImageManipulator,SaveFormat } from 'expo-image-manipulator';
import { getColors } from 'react-native-image-colors';
import ImageView from 'react-native-image-viewing';
import ImageCropPicker from 'react-native-image-crop-picker';
import Utilities from '../components/Utilities';



const CameraScreen = () => {
  const navigation = useNavigation();


  const route = useRoute();
  const { knownTableData, unknownColorsData,knownrgbtableData, setknownrgbtableData,unknownrgbtableData, setunknownrgbtableData, setKnownTable, setUnknownColorsData } = route.params;

  const [hasPermission, setHasPermission] = useState(null);
  const cameraRef = useRef(null);
  const [capturedImageUri, setCapturedImageUri] = useState('');
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [knownImages, setKnownKnownImages] = useState([]);
  const [unknownImages, setUnknownImages] = useState([]);
  const [isLoading, setIsLoading] = useState(false); //loding while firsr crop
  const [calculatingColors, setCalculatingColors] = useState(false); //loding while calculating the colors

  const [selectedImageIndex, setSelectedImageIndex] = useState(null);
  const [isImageViewVisible, setIsImageViewVisible] = useState(false);
  const [selectedImages, setSelectedImages] = useState([]);
  const [selectedImageUri, setSelectedImageUri] = useState(null);
  const [knownTable, settingKnownTable]=useState([]);
  const [unknownTable, settingUnKnownTable]=useState([]);


  useEffect(() => {
    (async () => {
      const { status } = await Camera.requestCameraPermissionsAsync();
      setHasPermission(status === 'granted');
    })();
  }, []);


//Take picture function takes the picture ,crops and sets knownImages and unknownImages  state vaiables,
  const takePicture = async () => {
    if (cameraRef.current) {
      const photo = await cameraRef.current.takePictureAsync();
      console.log('image uri:',photo.uri);
      setIsLoading(true);
      const { width: photoWidth, height: photoHeight } = photo;
      const cropSize = photoWidth / 5;
  console.log('cropSize in take picture function:', cropSize);
      const knownImageUris = [];
      const unknownImageUris = [];
  
      for (let i = 0; i < 5; i++) {
        for (let j = 0; j < 5; j++) {
          const croppedImage = await manipulateAsync(
            photo.uri,
            [{
              crop: {
                originX: j * cropSize,
                originY: i * cropSize,
                width: cropSize,
                height: cropSize
              }
            }],
            { compress: 1, format: SaveFormat.JPEG }
          );
          if (i < 3) {
            knownImageUris.push(croppedImage.uri);
          } else {
            if (!((i === 3 || i === 4) && (j === 0 || j === 4))) {
              unknownImageUris.push(croppedImage.uri);
            }
          }
        }
      } 
  
      setCapturedImageUri(photo.uri);
      setKnownKnownImages(knownImageUris);
      setUnknownImages(unknownImageUris);
      setIsModalVisible(true);
      console.log('knownImageUris:',knownImageUris);
      console.log('unknownImageUris',unknownImageUris);
      setIsLoading(false); 
     // calculateColor(knownImageUris, unknownImageUris);
    }
  }; 

//function to getColors from each image and make them in a  gruop 
  const calculateColor = async (knownImages, unknownImages) => {
    try {
      //First get colors from each Image 
      console.log("Calculating colors for known images...");
      const knownColors = await Promise.all(knownImages.map(uri => getColors(uri)));
      //console.log("Known images colors:", knownColors);

      console.log("Calculating colors for unknown images...");
      const unknownColors = await Promise.all(unknownImages.map(uri => getColors(uri)));
      //console.log("Unknown images colors:", unknownColors);

     const knowngroup1 = [];
     const knowngroup2 = [];
     const knowngroup3 = [];
     const knowngroup4 = [];
     const knowngroup5 = [];
     
     // Iterate through the known array and make groups by taking the avg color from  getColor function ^
     knownColors.forEach((color,index) => {
         // Determine the group based on index
         switch ((index+1) % 5) {
             case 1:
               knowngroup1.push(color.average); // Store average color in group1 array
                 break;
             case 2:
               knowngroup2.push(color.average); // Store average color in group2 array
                 break;
             case 3:
               knowngroup3.push(color.average); // Store average color in group3 array
                 break;
             case 4:
               knowngroup4.push(color.average); // Store average color in group4 array
                 break;
             case 0:
               knowngroup5.push(color.average); // Store average color in group5 array
                 break;
             default:
                 break;
         }
     });
     
           // Output the arrays
          //  console.log("Known Group 1:", knowngroup1);
          //  console.log("Known Group 2:", knowngroup2);
          //  console.log("Known Group 3:", knowngroup3);
          //  console.log("Known Group 4:", knowngroup4);
          //  console.log("Known Group 5:", knowngroup5);   

           //calling for the avg of hex numbers in each array.
          const group1Average = Utilities.calculateAverageColor(knowngroup1);
          const group2Average = Utilities.calculateAverageColor(knowngroup2);
          const group3Average = Utilities.calculateAverageColor(knowngroup3);
          const group4Average = Utilities.calculateAverageColor(knowngroup4);
          const group5Average = Utilities.calculateAverageColor(knowngroup5);

        // make an array containing all the avg.groups 
          const knownaverageColorsArray = [group1Average, group2Average, group3Average, group4Average, group5Average];
          console.log("Average Colors Array:", knownaverageColorsArray);

        // Convert average colors from hex to LAB
               const knownLABColorsArray = knownColors.map((hexColor,index) => {
          const rgbColor = Utilities.hexToRgb(hexColor.average);
         // console.log("RGB: [known]: ",rgbColor);
          const [l, a, b] = Utilities.rgb2lab(rgbColor);
            let CircleNo;
          switch (index % 5) {
            case 0:
              CircleNo = 0;
              break;
            case 1:
              CircleNo = 2;
              break;
            case 2:
              CircleNo = 4;
              break;
            case 3:
              CircleNo = 6;
              break;
            case 4:
              CircleNo = 8;
              break;
            default:
              CircleNo = null;
              break;
            }
            return { CircleNo, l, a, b};
        });
       // console.log("knownLabColorsArray:",knownLABColorsArray);

 //for Calculating the DeltaE in the same Array it self 
       Table1 = knownLABColorsArray.map(color => {
          const { CircleNo, l, a, b } = color;
          let E = Utilities.deltaE(knownLABColorsArray[0], color);
          return { CircleNo, l, a, b, E };
        });

 //for Calculating the DeltaE in the same Array it self 
       Table1 = knownLABColorsArray.map(color => {
          const { CircleNo, l, a, b } = color;
          let E = Utilities.deltaE(knownLABColorsArray[0], color);
          return { CircleNo, l, a, b, E };
        });

            const knwonrgb = knownaverageColorsArray.map((hexColor, index) => {
          const rgbclr = Utilities.hexToRgb(hexColor);
          return {
            groupNumber: index + 1,
            r: rgbclr[0],
            g: rgbclr[1],
            b: rgbclr[2]
          };
        });

       
        
      // Process known group (function)
      let known=knownaverageColorsArray.map((colors, index) => {
        const hsl = Utilities.hexToHSL(colors);
        return {
          groupNumber: index + 1,
          h: hsl.h,
          s: hsl.s,
          l: hsl.l,
          a: colors
        };
      });

     
     //Process the Unkown Part
      let unknowngroup1=[];
      let unknowngroup2=[];

      unknownColors.forEach((clr,index)  => {
        // Determine the group based on index
        if (index <3) {
          unknowngroup2.push(clr.average); // Store average color in group1 array for circles 1, 2, 3
        } else {
          unknowngroup1.push(clr.average); // Store average color in group2 array for circles 4, 5, 6
        }
    });
    
    // Output the arrays
    // console.log("Unknwon Group 1:", unknowngroup1);
    // console.log("Unknwon Group 2:", unknowngroup2);

    const unknowngroup1Average = Utilities.calculateAverageColor(unknowngroup1);
    const unknowngroup2Average = Utilities.calculateAverageColor(unknowngroup2);

    const unknownaverageColorsArray = [unknowngroup1Average, unknowngroup2Average]
   // console.log("unknownaverageColorsArray",unknownaverageColorsArray);


          //here we call the hex to rgb and then to lab function
           const unknownLabColorsArray = unknownColors.map(hexColor => {
            const rgbColor = Utilities.hexToRgb(hexColor.average); // use whichever needed avg,dominant,etc.
           // console.log("RGB: [unkown ]",rgbColor);
            const [l,a,b] = Utilities.rgb2lab(rgbColor);
            return {l,a,b}
          });
         // console.log("unknownLabColorsArray:",unknownLabColorsArray);
           //for Calculating the DeltaE in the same Array it self 
          Table2 = unknownLabColorsArray.map(color => {
          const { CircleNo, l, a, b } = color;
          let E = Utilities.deltaE(knownLABColorsArray[0], color);
          return { CircleNo, l, a, b, E };
        });

        const unknwonrgb = unknownaverageColorsArray.map((hexColor, index) => {
          const rgbclr = Utilities.hexToRgb(hexColor);
          return {
            groupNumber: index + 1,
            r: rgbclr[0],
            g: rgbclr[1],
            b: rgbclr[2]
          };
        });

              // console.log('unknwonrgb:',unknwonrgb);


      // Process unknown colors (function)
      let unknown=unknownaverageColorsArray.map((colors, index) => {
        const hsl = Utilities.hexToHSL(colors);
        return {
          groupNumber: index + 1,
          h: hsl.h,
          s: hsl.s,
          l: hsl.l,
          a: colors
        };
      });

         


//To pass back to home page  
      setKnownTable(Table1);
      setUnknownColorsData(Table2);
       setknownrgbtableData(knwonrgb);
       setunknownrgbtableData(unknwonrgb);



      // console.log('knownTable: in Calc function',knownTable);
      // console.log('unknownTable  in Calc function',unknownTable);
      // console.log('knownRGB Table in calculate function:', knownrgbtableData);
      // console.log('unknownRGB Table in calculate function:', unknownrgbtableData);
      setCalculatingColors(false);
      navigation.navigate('HomeScreen');
    } catch (error) {
      console.error("Error calculating colors:", error);
    }
  };

  const viewDetails = async () => {
    setCalculatingColors(true);
    await  calculateColor(knownImages, unknownImages);
   
    // console.log("knownTableData in viewdetals function",knownTableData);
    // console.log("unkowncolors/TableData in ViewDetails function:",unknownColorsData); 
    // if (knownTable.length === 0 || unknownTable.length === 0) {
      // Alert.alert('Validation Error', 'Both known and unknown data must be provided.');
      //console.log("not initialized");
     //} else {
      //navigation.navigate('HomeScreen');
       // knownTableData: knownTableData,
       // unknownColorsData: unknownColorsData
      
    //}
  };

/*const toGallery=()=>{
navigation.navigate("Gallery",{capturedImageUri,knownImages,unknownImages, croppedKnownImageUris,croppedUnknownImageUris});
};
*/

const openImageView = async (index, imageUri,isKnownImage) => {
  try {
    setSelectedImageIndex(index);
    setSelectedImageUri(imageUri);
    setIsImageViewVisible(true);

    // Open the cropping tool
    const croppedImage = await ImageCropPicker.openCropper({
      path: imageUri,
      // You can adjust cropping options as needed
      cropperToolbarTitle: 'Crop Image', // Set the title for the cropping toolbar
      cropperCircleOverlay: true, // Enable circular cropping
      mediaType: 'photo', // Specify the media type (photo or video)
    });

    const updatedImages = isKnownImage ? [...knownImages] : [...unknownImages];
    updatedImages[index] = croppedImage.path;
    if (isKnownImage) {
      setKnownKnownImages(updatedImages);
    } else {
      setUnknownImages(updatedImages);
    } 
    // Update the selected image URI with the cropped image path
    setSelectedImageUri(croppedImage.path);
  } catch (error) {
    console.error("Error opening Image: ", error);
  }
};

const closeImageView = () => {
  setIsImageViewVisible(false);
  setSelectedImageIndex(null);
  setSelectedImageUri(null);
};



  return (
    <View style={styles.container}>
      <StatusBar barStyle="dark-content" />
      {hasPermission && (
        <View style={styles.cameraContainer}>
          <Camera style={styles.camera} ref={cameraRef} ratio='1:1'>
            {/* Add circles grid here */}
            
            <View style={styles.circlesContainer}>
          {[...Array(5).keys()].map((row) => ( //known images 5*5 =15
            <View key={row} style={styles.row}>
              {[...Array(5).keys()].map((col) => {
                if (row < 3) {
                  return ( <View key={col} style={styles.circle}></View> );
                    } else { //unknown images (6)
                      if (col === 1 || col === 3) {
                        return null; // Skip rendering left and right circles
                      } else {
                        return (<View key={col} style={styles.circle}></View> );
                      }}})}
                </View>
              ))}
            </View>

          </Camera>
          <TouchableOpacity style={styles.captureButton} onPress={takePicture}>
            <MaterialIcons name="camera" size={40} color="black" />
          </TouchableOpacity>
          {isLoading && <ActivityIndicator size="large" color="white" />} 
          <Modal visible={isModalVisible} transparent={true} onRequestClose={() => setIsModalVisible(false)}>
  <View style={styles.modalContainer}>
      <Text style={{ fontWeight: "bold" , borderBottomColor:'black', borderBottomWidth:0.5}}> Images</Text>
      <View style={styles.imageGrid}>
        {/* Render known images in a 5x5 grid */}
        <Text style={{margin:5}}>Known Images</Text>
        {[...Array(3).keys()].map((row) => (
          <View key={row} style={styles.imageRow}>
            {[...Array(5).keys()].map((col) => {
              const index = row * 5 + col;
              return (
                <View key={index} style={[styles.capturedImageContainer, { opacity: index < knownImages.length ? 1 : 0 }]}>
                  {knownImages[index] && (
                    <TouchableOpacity key={index} onPress={() => openImageView(index, knownImages[index], true)}>
                      <Image
                        source={{ uri: knownImages[index] }}
                        style={styles.capturedImage}
                      />
                    </TouchableOpacity>
                  )}
                </View>
              );
            })}
          </View>
        ))}
        {/* Render unknown images in a 2x3 grid */}
        <Text style={{margin:5}}>Unknown Images</Text>
        {[...Array(2).keys()].map((row) => (
          <View key={row} style={styles.imageRow}>
            {[...Array(3).keys()].map((col) => {
              const index = row * 3 + col;
              return (
                <View key={index} style={[styles.capturedImageContainer, { opacity: index < unknownImages.length ? 1 : 0 }]}>
                  {unknownImages[index] && (
                    <TouchableOpacity key={index} onPress={() => openImageView(index, unknownImages[index] ,false)}>
                      <Image
                        source={{ uri: unknownImages[index] }}
                        style={styles.capturedImage}
                      />
                    </TouchableOpacity>
                  )}
                </View>
              );
            })}
          </View>
        ))}
      </View>
      <View style={{ margin: 5 }}></View>
      <Button title="CLOSE" onPress={() => setIsModalVisible(false)} color={"black"} />
      <View style={{ margin: 10 }}></View>
      <Button title="GET VALUES " onPress={viewDetails} color={"black"} />
    
    {calculatingColors  && <ActivityIndicator size="large" color="black   " />} 
  </View>
</Modal>

<Modal visible={isImageViewVisible} transparent={true} onRequestClose={closeImageView}>
  <View style={styles.imageViewModal}>
  <View style={styles.circleOverlay}></View>
    <ImageView
      images={[{ uri: selectedImageUri }]} // Pass the selected image URI to ImageView
      imageIndex={selectedImageIndex}
      visible={isImageViewVisible}
      onRequestClose={closeImageView}
    />
    
  </View>
</Modal>




</View>
)}</View>
  );
};

const { width } = Dimensions.get('window');


const circleSize = width /5; 

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FAD9BF',
  },
  cameraContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  camera: {
    width: width,
    height: width+15,
  },
  circlesContainer: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    justifyContent:'space-between',
    alignItems: 'center',
  },
  row: {
    flexDirection: 'row',
  },
  circle: {
    width: circleSize,
    height: circleSize,
    borderRadius: circleSize / 2,
    backgroundColor: 'transparent',
    borderColor: 'black',
    borderWidth: 15, // Adjust the border width as needed
    margin:2,
  },
  captureButton: {
    position: 'absolute',
    bottom: 20,
    alignSelf: 'center',
  },

  modalContainer: {
    width:350,
    height: 550, // Adjust the height to fit content
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    borderRadius: 5,
    marginLeft:50,
    marginTop: 200, // Adjust marginTop as needed
  },
 
  imageGrid: {
    flexDirection: 'column',
    flexWrap: 'wrap',
    alignItems: 'center',
    justifyContent: 'center',
  },
  imageRow: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  capturedImageContainer: {
    width: 60,
    height: 60,
    margin: 3, // Adjust margin as needed
  },
  capturedImage: {
    width: '100%',
    height: '100%',
    borderRadius: 100,
  },

  imageViewModal: {
    flex: 1,
    //backgroundColor: 'rgba(0, 0, 0, 0.7)',
    justifyContent: 'center',
    alignItems: 'center',
  },
  circleOverlay: {
    position: 'absolute',
    top: 232, // Adjust as needed
    left: 0, // Adjust as needed
    width:430,
    height: 430,
    borderRadius: 250,
    backgroundColor: 'transparent',
    borderWidth: 2,
    borderColor: 'white',
   // transform: [{ translateX: -50 }, { translateY: -50 }],
  },
  
});


function hexToHSL(hex) {
  // Remove '#' if it exists
  hex = hex.replace('#', '');

  // Parse hex to RGB
  const r = parseInt(hex.substring(0, 2), 16) / 255;
  const g = parseInt(hex.substring(2, 4), 16) / 255;
  const b = parseInt(hex.substring(4, 6), 16) / 255;

  // Find greatest and smallest channel values
  const max = Math.max(r, g, b);
  const min = Math.min(r, g, b);

  // Calculate lightness
  let l = (max + min) / 2;

  // Calculate saturation
  let s = 0;
  if (max !== min) {
      s = l > 0.5 ? (max - min) / (2 - max - min) : (max - min) / (max + min);
  }

  // Calculate hue
  let h = 0;
  if (max === r) {
      h = (g - b) / (max - min) + (g < b ? 6 : 0);
  } else if (max === g) {
      h = (b - r) / (max - min) + 2;
  } else {
      h = (r - g) / (max - min) + 4;
  }
  h /= 6;

  // Convert hue to degrees
  h *= 360;

  // Round values and return as object
  return { h: Math.round(h), s: Math.round(s * 100), l: Math.round(l * 100) };
}


const calculateAverageColor = (colors) => {
  // Separate the red, green, and blue components
  const reds = colors.map(color => parseInt(color.substring(1, 3), 16));
  const greens = colors.map(color => parseInt(color.substring(3, 5), 16));
  const blues = colors.map(color => parseInt(color.substring(5, 7), 16));

  // Calculate the average of each component
  const avgRed = Math.round(reds.reduce((a, b) => a + b, 0) / colors.length);
  const avgGreen = Math.round(greens.reduce((a, b) => a + b, 0) / colors.length);
  const avgBlue = Math.round(blues.reduce((a, b) => a + b, 0) / colors.length);

  // Convert the average components back to hexadecimal
  const avgHex = `#${avgRed.toString(16).padStart(2, '0')}${avgGreen.toString(16).padStart(2, '0')}${avgBlue.toString(16).padStart(2, '0')}`;

  return avgHex;
};


function hexToRgb(hex) {
  // Remove '#' if it exists
  hex = hex.replace('#', '');

  // Parse hex to RGB
  const r = parseInt(hex.substring(0, 2), 16);
  const g = parseInt(hex.substring(2, 4), 16);
  const b = parseInt(hex.substring(4, 6), 16);

  // Return RGB values as an array
  return [r, g, b];
}



function rgb2lab(rgb){
  var r = rgb[0] / 255,
      g = rgb[1] / 255,
      b = rgb[2] / 255,
      x, y, z;

  r = (r > 0.04045) ? Math.pow((r + 0.055) / 1.055, 2.4) : r / 12.92;
  g = (g > 0.04045) ? Math.pow((g + 0.055) / 1.055, 2.4) : g / 12.92;
  b = (b > 0.04045) ? Math.pow((b + 0.055) / 1.055, 2.4) : b / 12.92;

  x = (r * 0.4124 + g * 0.3576 + b * 0.1805) / 0.95047;
  y = (r * 0.2126 + g * 0.7152 + b * 0.0722) / 1.00000;
  z = (r * 0.0193 + g * 0.1192 + b * 0.9505) / 1.08883;

  x = (x > 0.008856) ? Math.pow(x, 1/3) : (7.787 * x) + 16/116;
  y = (y > 0.008856) ? Math.pow(y, 1/3) : (7.787 * y) + 16/116;
  z = (z > 0.008856) ? Math.pow(z, 1/3) : (7.787 * z) + 16/116;

  return [(116 * y) - 16, 500 * (x - y), 200 * (y - z)].map(value => parseFloat(value.toFixed(2)));
}


/*

  const cropAgain = async () => {
    setIsLoading(true);
  
    const croppedKnownImageUris = [];
    const croppedUnknownImageUris = [];
    const cropSizeMultiplier = 0.5;
  
    try {
      // Get image dimensions concurrently for known and unknown images
      const knownImageDimensions = await Promise.all(
        knownImages.map( (imageUri) => {
          console.log("Processing known image:", imageUri);
         // const { width, height } = 614.4;
          return { width:614.4, height:614.4};
        })
      );
  
      const unknownImageDimensions = await Promise.all(
        unknownImages.map( (imageUri) => {
          console.log("Processing unknown image:", imageUri);
         // const { width, height } =614.4;
          return { width:614.4, height:614.4 };
        })
      );
  
      // Process known and unknown images in separate loops
     // console.log(knownImageDimensions);
      for (let i = 0; i < knownImageDimensions.length; i++) {
        const { width, height } = knownImageDimensions[i];
        const imageUri = knownImages[i];
  
        const cropSize = Math.min(width, height) * cropSizeMultiplier;
        const cropX = (width - cropSize) / 2;
        const cropY = (height - cropSize) / 2;
  
        const croppedImage = await manipulateAsync(
          imageUri,
          [{
            crop: {
              originX: cropX,
              originY: cropY,
              width: cropSize,
              height: cropSize
            }
          }],
          { compress: 1, format: SaveFormat.JPEG }
        );
        croppedKnownImageUris.push(croppedImage.uri);
      }
  
      for (let i = 0; i < unknownImageDimensions.length; i++) {
        const { width, height } = unknownImageDimensions[i];
        const imageUri = unknownImages[i];
  
        const cropSize = Math.min(width, height) * cropSizeMultiplier;
        const cropX = (width - cropSize) / 2;
        const cropY = (height - cropSize) / 2;
  
        const croppedImage = await manipulateAsync(
          imageUri,
          [{
            crop: {
              originX: cropX,
              originY: cropY,
              width: cropSize,
              height: cropSize
            }
          }],
          { compress: 1, format: SaveFormat.JPEG }
        );
        croppedUnknownImageUris.push(croppedImage.uri);
       
      }
      setIsLoading(false);
      setKnownKnownImages(croppedKnownImageUris);
      setUnknownImages(croppedUnknownImageUris);

      calculateColor(croppedKnownImageUris, croppedUnknownImageUris);
    } catch (error) {
      console.error("Error cropping images:", error);
      setIsLoading(false);
    }
  }; 
   */ 
export default CameraScreen;



